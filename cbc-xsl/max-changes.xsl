<!--
  Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
  
  Maximum number of changes filter.

  This filter analyses the character by character comparison to determine whether the regions
  of text being compared have too much change for the result to be sensibly understood.
  In these cases, replace the character by character result for the given text region with a
  deleted version of the original text and an inserted version of the new text.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1" version="2.0">

  <!--
    The maximum number of changes (insertions + deletions) to allow in a given text region.
  -->
  <xsl:param name="max-number-of-changes" as="xs:string" select="'2'" />

  <xsl:template match="@* | node()" mode="#default copy">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--
    Match a given region of text, check if it contains too much change, and then
    produce the appropriate output as discussed above.
  -->
  <xsl:template match="*[delete or insert or unchanged]">
    <xsl:variable name="number-of-changes" select="count(*[self::insert or self::delete])" as="xs:integer"/>
    <xsl:variable name="max-changes" select="xs:integer($max-number-of-changes)" as="xs:integer"/>
    
    <xsl:choose>
      <xsl:when test="$number-of-changes gt $max-changes">
        <xsl:call-template name="convertToOneDeleteAndInsert" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="copy"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="convertToOneDeleteAndInsert">
    <xsl:copy>
      <xsl:element name="delete">
        <xsl:apply-templates select="*[self::delete or self::unchanged]/text()" />
      </xsl:element>
      <xsl:element name="insert">
        <xsl:apply-templates select="*[self::insert or self::unchanged]/text()" />
      </xsl:element>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
