<!--
  Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
  
  Transform the processed character by character result into the DeltaXML deltaV2 format.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
                version="2.0">
  
  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--
    If chunking is enabled 'unwrap' the chunks and separator elements
  -->
  <xsl:template match="chunk | separator">
    <xsl:apply-templates select="node()" />
  </xsl:template>
  
  <!-- 
    Convert a deleted region of text to a textGroup
  -->
  <xsl:template match="delete">
    <xsl:element name="deltaxml:textGroup">
      <xsl:attribute name="deltaxml:deltaV2" select="'A'" />
      <xsl:element name="deltaxml:text">
        <xsl:attribute name="deltaxml:deltaV2" select="'A'" />
        <xsl:apply-templates select="text()" />
      </xsl:element>
    </xsl:element>
  </xsl:template>
  
  <!--
    Convert an inserted region of text to a textGroup
  -->
  <xsl:template match="insert">
    <xsl:element name="deltaxml:textGroup">
      <xsl:attribute name="deltaxml:deltaV2" select="'B'" />
      <xsl:element name="deltaxml:text">
        <xsl:attribute name="deltaxml:deltaV2" select="'B'" />
        <xsl:apply-templates select="text()" />
      </xsl:element>
    </xsl:element>    
  </xsl:template>
  
  <!--
    Convert an unchanged region of text into its underpinning text
  -->
  <xsl:template match="unchanged">
    <xsl:apply-templates select="text()" />
  </xsl:template>
  
</xsl:stylesheet>
