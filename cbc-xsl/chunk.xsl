<!--
  Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
  
  Chunk the result into text regions which can then be independently analysed by the 
  following filters.
  
  This filter splits the result into a sequence of 'chunk' elements that are separated
  by shared whitespace 'separator' elements.
  
  Note the other character by character filters are designed to work regardless of
  whether this chunking filter is run. 
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1" version="2.0">

  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--
    Split the result into a sequence of 'chunk' elements that are separated by
    shared whitespace 'separator' elements.
  -->
  <xsl:template match="deltaxml:cbcResult">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:for-each-group select="node()"
        group-adjacent="normalize-space(string(.))='' and deltaxml:groupkey(.)='A=B'">
        <xsl:variable name="isSharedWhitespaceSeparator" select="current-grouping-key()"
          as="xs:boolean"/>
        <xsl:choose>
          <xsl:when test="$isSharedWhitespaceSeparator">
            <xsl:element name="separator">
              <xsl:apply-templates select="current-group()"/>
            </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <xsl:element name="chunk">
              <xsl:apply-templates select="current-group()"/>
            </xsl:element>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each-group>
    </xsl:copy>
  </xsl:template>

  <!--
    The grouping key is the deltaV2 value associated with the given node (character). 
  -->
  <xsl:function name="deltaxml:groupkey" as="xs:string">
    <xsl:param name="node" as="node()"/>
    <xsl:choose>
      <xsl:when test="$node/@deltaxml:deltaV2">
        <xsl:value-of select="$node/@deltaxml:deltaV2"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'A=B'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>
