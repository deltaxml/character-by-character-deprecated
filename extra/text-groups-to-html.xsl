<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2013 DeltaXML Ltd. All rights reserved.
  
  A simple filter for converting deltaxml:devltaV2 text changes into html <ins> and <del>
  elements, in order to simplify the presentation of the character by character processing 
  results. Note it adds 'red' and 'green' CSS background colour styling to the <del> and 
  <ins> elements respectively.
  
  This filter is NOT intended for general processing.
  (1) it strips all other deltaxml:markup from the code.
  (2) no effort is taken to process element level changes, which will occur in general.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:cbc="java:CharacterByCharacter" version="2.0" exclude-result-prefixes="#all">

  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="deltaxml:textGroup">
    <xsl:apply-templates select="node()"/>
  </xsl:template>

  <xsl:template match="deltaxml:text[@deltaxml:deltaV2='A']">
    <xsl:element name="del" namespace="{namespace-uri(/*)}">
      <xsl:apply-templates select="node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="deltaxml:text[@deltaxml:deltaV2='B']">
    <xsl:element name="ins" namespace="{namespace-uri(/*)}">
      <xsl:apply-templates select="node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="deltaxml:* | @deltaxml:*"/>

  <!--
    Add some 'red' and 'green' CSS background colour highlighting to the HTML 'del'
    and 'ins' elements respectively.
  -->
  <xsl:template match="*:head[not(*:style)]">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*, node()"/>
      <xsl:element name="style" namespace="{namespace-uri(/*)}">
        <xsl:attribute name="type" select="'text/css'"/>
        <xsl:text>del { background-color: #F55; } </xsl:text>
        <xsl:text>ins { background-color: #90EE90; }</xsl:text>
      </xsl:element>
    </xsl:copy>
  </xsl:template>

  <!-- 
    Note this template adds the CSS styling of the ins and del elements regardless of
    whether these elements already had that style attached.
    
    It would be better to determine the styling of the CSS externally, but this works
    for this sample code.
  -->
  <xsl:template match="*:style">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*, node()"/>
      <xsl:text>del { background-color: #F55; } </xsl:text>
      <xsl:text>ins { background-color: #90EE90; }</xsl:text>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
