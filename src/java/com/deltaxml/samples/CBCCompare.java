// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.sf.saxon.s9api.Serializer;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FilterClassInstantiationException;
import com.deltaxml.core.FilterConfigurationException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.DuplicateStepNameException;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;

public class CBCCompare {
  
  public static void main(String[] args) throws ParserInstantiationException, FilterConfigurationException,
      ComparatorInstantiationException, FilterClassInstantiationException, ComparisonException, FilterProcessingException,
      PipelineLoadingException, PipelineSerializationException, LicenseException, FileNotFoundException,
      PipelinedComparatorError, IOException, ComparisonCancelledException {
    
    // initialise files
    File f1= new File(args[0]);
    File f2= new File(args[1]);
    File result= new File(args[2]);

    DocumentComparator dc= new DocumentComparator();
    
    dc.setOutputProperty(Serializer.Property.INDENT, "no");
    dc.setOutputProperty(Serializer.Property.METHOD, "xml");

    // Disable element splitting as this is not applied in the sample that uses the pipelined comparator
    dc.getResultReadabilityOptions().setElementSplittingEnabled(false);
    
    // Disable orphaned word detection as this is not applied in the sample that uses the pipelined comparator
    dc.getResultReadabilityOptions().setOrphanedWordDetectionEnabled(false);
    
    FilterStepHelper fsHelper= dc.newFilterStepHelper();
    FilterChain outfilters= fsHelper.newFilterChain();
 
    try {
      outfilters.addStep(fsHelper.newSingleStepFilterChain(new File("cbc-compare-dc.xsl"), "cbc-compare-dc"));
      dc.setExtensionPoint(ExtensionPoint.OUTPUT_FINAL, outfilters);

    } catch (DuplicateStepNameException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalStateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    dc.compare(f1, f2, result);

  }
}
