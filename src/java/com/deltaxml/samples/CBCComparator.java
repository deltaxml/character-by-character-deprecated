// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmNode;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FilterClassInstantiationException;
import com.deltaxml.core.FilterConfigurationException;
import com.deltaxml.core.FilterParameterizationException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.DuplicateStepNameException;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.PipelinedComparatorS9;

public class CBCComparator {
  
  public static NodeInfo compare(XPathContext context, NodeInfo nodeA, NodeInfo nodeB, boolean chunking, String maxchanges) throws ParserInstantiationException, FilterConfigurationException,
      ComparatorInstantiationException, FilterClassInstantiationException, ComparisonException, FilterProcessingException,
      PipelineLoadingException, PipelineSerializationException, LicenseException, FileNotFoundException,
      PipelinedComparatorError, IOException, ComparisonCancelledException {

    Processor proc= (Processor)context.getConfiguration().getProcessor();
    PipelinedComparatorS9 pc = new PipelinedComparatorS9(proc);
    
    boolean chunking_val=true;
    String maxchanges_val="2";
    if (chunking) {
      chunking_val=chunking;
    }
    
    if(!maxchanges.isEmpty()) {
      maxchanges_val=maxchanges;
    }
    
    pc.setOutputProperty(Serializer.Property.INDENT, "yes");
    pc.setOutputProperty(Serializer.Property.METHOD, "xml");
    
    FilterStepHelper fsHelper= pc.newFilterStepHelper();
    FilterChain outfilters= fsHelper.newFilterChain();

    try {
      //Apply red-green normalisation to the comparison result
      outfilters.addStep(fsHelper.newFilterStep(new File("dx2-red-green-outfilter.xsl"), "dx2-red-green-outfilter"));
      
      //Optionally chunk the result into 'shared' space seperated regions of text
      if (chunking_val) {
        outfilters.addStep(fsHelper.newFilterStep(new File("cbc-xsl/chunk.xsl"), "chunk"));
      }
      
      //Merge adjacent changes that are of the same type into an 'insert', a 'delete', or an 'unchanged' block, as appropriate.
      outfilters.addStep(fsHelper.newFilterStep(new File("cbc-xsl/merge-adjacent-changes.xsl"), "merge-adjacent-changes"));
      
      /*When more than an acceptable amount of change is detected within a
      chunk (or the whole comparison if chunking is off) replace the
      contents of the chunk with one 'delete' and one 'insert' block. 
      
      Other filters updating the nature of the result could either be added
      in addition to this filter or replace this filter at this point in
      the pipeline.*/
      outfilters.addStep(fsHelper.newFilterStep(new File("cbc-xsl/max-changes.xsl"), "max-changes"));
      outfilters.getStep("max-changes").setParameterValue("max-number-of-changes", maxchanges_val);
      
      //Convert the result into a form that is required for the output of this result
      outfilters.addStep(fsHelper.newFilterStep(new File("cbc-xsl/convert-to-deltaV2.xsl"), "convert-to-deltaV2"));
      
      //Apply red-green normalisation to the final result.
      outfilters.addStep(fsHelper.newFilterStep(new File("dx2-red-green-outfilter.xsl"), "dx2-red-green-outfilter1"));

      pc.setOutputFilters(outfilters);
    } catch (DuplicateStepNameException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IllegalStateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (FilterParameterizationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    XdmNode xNodeA= new XdmNode(nodeA);
    XdmNode xNodeB= new XdmNode(nodeB);
    NodeInfo nodeInfo= pc.compare(xNodeA, xNodeB).getUnderlyingNode();
    
    return nodeInfo;
  }
}
