<!--
Copyright (c) 2025 DeltaXML Ltd. All rights reserved.

Character By Character sample build file.
-->
<project name="Character By Character" default="run" basedir=".">

  <property name="install-dir" location="../.." />

  <path id="deltaxml.jar.id">
    <fileset dir="${install-dir}" includes="deltaxml*.jar" excludes="deltaxml-rest*.jar,deltaxml-gui*.jar"/>
  </path>
  <pathconvert property="deltaxml.jar.abs" refid="deltaxml.jar.id" />
  <property name="deltaxml-jar" value="${deltaxml.jar.abs}" relative="yes" basedir="."/>

  <property name="saxon-jar" location="${install-dir}/saxon9pe.jar" />
  <property name="resolver-jar" location="${install-dir}/resolver.jar" />
  <property name="icu4j-jar" location="${install-dir}/icu4j.jar" />
  <property name="xerces-jar" location="${install-dir}/xercesImpl.jar" />

  <property name="classes" location="class" />

  <target name="run" depends="run-dxp,run-dc">

  </target>

  <target name="run-dxp">
    <compare in1="input1.html" in2="input2.html" out="output-dxp.xml" /> 
    <convertTextGroupsToHtml in="output-dxp.xml" out="output-dxp.html" /> 
  </target>

  <macrodef name="compare">
    <attribute name="in1" />
    <attribute name="in2" />
    <attribute name="out" />
    <sequential>
      <echo message="comparing @{in1} with @{in2}" />
      <java jar="${deltaxml-jar}" fork="yes" failonerror="yes"> 
        <arg value="compare"/>
        <arg value="cbc"/>
        <arg value="@{in1}"/>
        <arg value="@{in2}"/>
        <arg value="@{out}"/>
      </java>
    </sequential>
  </macrodef>

  <macrodef name="convertTextGroupsToHtml">
    <attribute name="in" />
    <attribute name="out" />
    <sequential>
      <echo message="converting text group changes to html markup" />
      <java jar="${saxon-jar}" fork="yes" failonerror="yes"> 
        <arg value="-config:extra/saxon.config"/>
        <arg value="-xsl:extra/text-groups-to-html.xsl"/>
        <arg value="-s:@{in}"/>
        <arg value="-o:@{out}"/>
      </java>
    </sequential>
  </macrodef>

  <target name="run-dc" depends="compile">
    <java classname="com.deltaxml.samples.CBCCompare"
      fork="yes"
      failonerror="true">
      <classpath>
        <pathelement location="bin"/>
        <path refid="deltaxml.jar.id"/>
        <pathelement location="${saxon-jar}"/>
        <pathelement location="${icu4j-jar}"/>
        <pathelement location="${resolver-jar}"/>
        <pathelement location="${xerces-jar}"/>
      </classpath>
      <arg value="input1.html"/>
      <arg value="input2.html"/>
      <arg value="output-dc.xml"/>
    </java>
    <convertTextGroupsToHtml in="output-dc.xml" out="output-dc.html" /> 
  </target>

  <target name="compile">
    <mkdir dir="bin"/>
    <javac srcdir="src/java"
      destdir="bin"
      includeantruntime="false"
      debug="on">
      <classpath>
        <path refid="deltaxml.jar.id"/>
        <pathelement location="${saxon-jar}"/>
        <pathelement location="${xerces-jar}"/>
      </classpath>
    </javac>
  </target>

  <target name="clean" description="Reset the sample">
    <delete>
      <fileset dir="." includes="output-dxp.xml,output-dxp.html,output-dc.xml,output-dc.html" />
    </delete>
    <delete dir="bin"/>
  </target>

</project>
