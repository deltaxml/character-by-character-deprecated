# Character By Character Comparison
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---
How character by character comparison can be performed using the Pipelined Comparator or Document Comparator.

This document describes how to run the sample. For concept details see: [Character by Character Comparison](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/character-by-character-comparison)

The sample code illustrates two methods of performing character by character comparison. The first method uses the Pipelined Comparator, and specifies input and output filters with a DXP file. The second method uses the API exposed by the Document Comparator.  

If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output-dxp.xml and output-dc.xml files and overwrite the output-dxp.html and output.html files. Here, the output-dxp.xml and output-dc.xml files represent the result of the comparison, which has then been converted into HTML markup.

	ant run
	
## Pipelined Comparator
To run just the Pipelined Comparator sample and produce the output files

	ant run-dxp
	
## Document Comparator
To run just the Document Comparator sample and produce the output files

	ant run-dc

Note that the HTML markup script is not formally part of the character by character sample, but is included to simplify the presentation of the sample results. It converts the deltaxml:textGroup additions and deletions into HTML insertions and deletions respectively, and removes all other DeltaXML markup. It is not intended to be used as a general purpose DeltaV2 to HTML filter, as discussed in the 'extra' directory's Read Me document.
Running the sample from a command line
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory.

## Pipelined Comparator
Replace x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar

	java -jar ../../deltaxml-x.y.z.jar compare cbc input1.html input2.html output-dxp.xml
	
## Document Comparator
Replace x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar

	mkdir bin
	javac -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/CBCCompare.java
	javac -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/CBCComparator.java
	java -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.CBCCompare inpu1.xml input2.xml output-dc.xml
	
Having produced the comparison result it may be worth adding the HTML markup, as discussed above. This can be achieved by issuing the following command.

	java -jar ../../saxon9pe.jar -config:extra/saxon.config -xsl:extra/text-groups-to-html.xsl -s:output.xml -o:output.html

To clean up the sample directory, run the following Ant command.

	ant clean
