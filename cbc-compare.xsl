<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (c) 2013 DeltaXML Ltd. All rights reserved.

This is the main Character by Character output filter.

It is intended to be included as an output filter in a user's own comparison pipeline.
Please refer to the sample ReadMe for details on how to do this, as it requires
supporting files.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:deltajava="java:com.deltaxml.ext.xslt.saxon.Comparison" version="2.0">

  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--
    Compare the elements of modified deltaxml:textGroups using the character by
    character comparison function.
  -->
  <xsl:template match="deltaxml:textGroup[@deltaxml:deltaV2='A!=B']">
    <!-- Convert the modified textGroup into character by character comparison inputs -->
    <xsl:variable name="a"
      select="deltaxml:generate-input(string(deltaxml:text[@deltaxml:deltaV2='A']))"
      as="element(deltaxml:cbcResult)"/>
    <xsl:variable name="b"
      select="deltaxml:generate-input(string(deltaxml:text[@deltaxml:deltaV2='B']))"
      as="element(deltaxml:cbcResult)"/>

    <!-- Perform the character by character comparison -->
    <xsl:variable name="delta" as="node()"
      select="deltajava:compare($a, $b, resolve-uri('cbc-pipeline.dxp', static-base-uri()))"/>

    <!-- Replace this deltaxml:textGroup with the contents of the comparison result -->
    <xsl:apply-templates select="$delta/deltaxml:cbcResult/node()"/>
  </xsl:template>

  <!--
    This function takes a string and produces a deltaxml:cbcResult element that contains
    a sequence of character 'keyed' elements, one for each character in the string.
  -->
  <xsl:function name="deltaxml:generate-input" as="element(deltaxml:cbcResult)">
    <xsl:param name="input" as="xs:string"/>

    <xsl:element name="deltaxml:cbcResult">
      <xsl:for-each select="string-to-codepoints($input)">
        <xsl:element name="c">
          <xsl:attribute name="deltaxml:key" select="current()" />
          <xsl:value-of select="codepoints-to-string(current())"/>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:function>

</xsl:stylesheet>
